FROM ubuntu:trusty
RUN sudo apt-get -y update
RUN sudo apt-get -y upgrade
RUN sudo apt-get install -y sqlite3 libsqlite3-dev

RUN mkdir /var/check-13
WORKDIR /var/check-13

COPY ./ ./

CMD [ "./main" ]
