#!/bin/bash

value=$1
count=0

while [ $(sqlite3 'check13.db' "SELECT $value % 13;") -eq 0 ]
do
    count=$(sqlite3 'check13.db' "SELECT $count + 1;")
    value=$(sqlite3 'check13.db' "SELECT $value / 13;")
done
echo "13,$count,sql, SELECT pain FROM languageTable; DROP TABLE User;"
